package com.example.demodropbox;

import java.io.File;
import java.io.OutputStream;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

public class DownloadFile extends AsyncTask<Void, Long, Boolean> {

	private DropBoxActivity	mContext;
	private DropboxAPI<?>	mDropBoxApi;
	private String			mFilePath;
	private ProgressDialog	mDialog;
	private OutputStream	mFileStram;

	public DownloadFile(DropBoxActivity mainActivity, DropboxAPI<AndroidAuthSession> api, String filepath, OutputStream stream) {
		mContext = mainActivity;
		mDropBoxApi = api;
		mFilePath = filepath;
		mFileStram = stream;
		mDialog = new ProgressDialog(mContext);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		File file = new File(mFilePath);
		mDialog.setMessage("Downloading :" + file.getName());
		mDialog.setCancelable(false);
		mDialog.setSecondaryProgress(ProgressDialog.STYLE_SPINNER);
		mDialog.show();
	}

	@Override
	protected Boolean doInBackground(Void... params) {

		try {
			mDropBoxApi.getFileStream(mFilePath, null);
			mDropBoxApi.getFile(mFilePath, null, mFileStram, null);
		} catch (DropboxException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		mDialog.dismiss();
		Toast.makeText(mContext, " file downoaded", Toast.LENGTH_SHORT).show();
	}

}
