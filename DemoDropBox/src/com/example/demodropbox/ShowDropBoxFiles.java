package com.example.demodropbox;

import java.util.ArrayList;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ShowDropBoxFiles extends AsyncTask<Void, Integer, Boolean> {
	private String				mFilepath;
	private DropboxAPI<?>		mDropboxAPi;
	private ListView			mListview;
	private ArrayList<FileInfo>	mFileinfo;
	private DropBoxActivity		mMainActivity;
	ProgressDialog				mDialog;
	private boolean				mIsFile;

	public ShowDropBoxFiles(DropboxAPI<AndroidAuthSession> mDropBoxApi, String filepath, DropBoxActivity context,
			ArrayList<FileInfo> fileinfo) {
		mFilepath = filepath;
		mDropboxAPi = mDropBoxApi;
		mFileinfo = fileinfo;
		mMainActivity = context;
		mDialog = new ProgressDialog(mMainActivity);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mDialog.setMessage("Getting file list. Please wait...");
		mDialog.setCancelable(false);
		mDialog.show();
		mMainActivity.setDialogForFileList();
		mListview = mMainActivity.getListView();
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			Entry directory = mDropboxAPi.metadata(mFilepath, 1000, null, true, null);
			if (directory.isDir) {
				Log.w("dan", "FOLDER:::");
				int i = 0;
				Entry entries;
				FileInfo info = null;
				ArrayList<Entry> files = new ArrayList<Entry>();
				ArrayList<FileInfo> dir = new ArrayList<FileInfo>();

				dir.add(new FileInfo(mFilepath + "/..", "/.."));
				try {
					entries = mDropboxAPi.metadata(mFilepath, 100, null, true, null);
					for (Entry e : entries.contents) {
						if (!e.isDeleted) {
							files.add(e);
							String filename = files.get(i).path.substring(files.get(i).path.lastIndexOf("/") + 1,
									files.get(i).path.length());
							directory = mDropboxAPi.metadata(new String(files.get(i).path), 1000, null, true, null);
							if (directory.isDir)
								info = new FileInfo(new String(files.get(i).path), "/" + filename);
							else
								info = new FileInfo(new String(files.get(i).path), filename);
							dir.add(info);
							i++;
						}
					}
				} catch (DropboxException e1) {
					e1.printStackTrace();
				}
				mFileinfo.addAll(dir);
				mIsFile = false;
			} else {
				Log.w("dan", " NOT FOLDER:::");
				mIsFile = true;
			}
			return true;
		} catch (DropboxException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		mDialog.dismiss();
		if (!mIsFile) {
			ArrayList<String> names = new ArrayList<String>();
			for (FileInfo fi : mFileinfo) {
				names.add(fi.getFileName());
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mMainActivity.getBaseContext(),
					android.R.layout.simple_list_item_1, names);
			mListview.setAdapter(arrayAdapter);
			if (mMainActivity.getDialogStatus())
				mMainActivity.dismissDialog();
			mMainActivity.showDialog();

		} else {
			mMainActivity.downLoadFile(mFilepath);
		}
	}


}
