package com.example.demodropbox;

public class FileInfo {

	private String	mFileName, mFilePath;

	public String getFileName() {
		return mFileName;
	}

	public void setFileName(String mFileName) {
		this.mFileName = mFileName;
	}

	public String getFilePath() {
		return mFilePath;
	}

	public void setFilePath(String mFilePath) {
		this.mFilePath = mFilePath;
	}

	public FileInfo(String filepath, String filename) {
		super();
		this.mFileName = filename;
		this.mFilePath = filepath;
	}
}
