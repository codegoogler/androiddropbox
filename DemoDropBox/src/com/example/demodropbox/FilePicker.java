package com.example.demodropbox;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class FilePicker extends Builder implements OnItemClickListener, OnClickListener {

	private Button					mButton;
	private DropBoxActivity			mActivity;

	private File					mCurrentFolder;
	private Stack<File>				mPrevFolders;
	private FilePickerListAdapter	mAdapter;
	private View					mView;
	private ListView				mListView;
	private AlertDialog				mDialog;

	public FilePicker(DropBoxActivity dropBoxActivity) {
		super(dropBoxActivity);
		mActivity = dropBoxActivity;

		setTitle("File picker");
		setCancelable(false);
		LayoutInflater inflater = mActivity.getLayoutInflater();
		mView = inflater.inflate(R.layout.file_picker, null);
		setView(mView);
		initContorls();
		setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub

			}
		});
		loadFolder(Environment.getExternalStorageDirectory());
		mDialog = create();
		mDialog.show();

	}

	private void initContorls() {
		mListView = (ListView) mView.findViewById(R.id.listview);
		mListView.setOnItemClickListener(this);
		mAdapter = new FilePickerListAdapter(mActivity);
		mListView.setAdapter(mAdapter);

		mButton = (Button) mView.findViewById(R.id.button1);
		mButton.setOnClickListener(this);

		mPrevFolders = new Stack<File>();
	}

	private void loadFolder(File folder) {
		assert folder.isDirectory();
		setTitle(folder.getName());

		mCurrentFolder = folder;

		ProgressDialog progressDialog = ProgressDialog.show(mActivity, "", "Loading...", true);
		ArrayList<File> adapterFiles = mAdapter.getFiles();
		adapterFiles.clear();
		adapterFiles.addAll(Arrays.asList(folder.listFiles()));
		mAdapter.notifyDataSetChanged();

		progressDialog.dismiss();
	}

	private class FilePickerListAdapter extends BaseAdapter {
		private final LayoutInflater	mInflater;
		private final ArrayList<File>	mFiles;

		public FilePickerListAdapter(Context context) {
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mFiles = new ArrayList<File>();
		}

		public ArrayList<File> getFiles() {
			return mFiles;
		}

		@Override
		public int getCount() {
			return mFiles.size();
		}

		@Override
		public File getItem(int position) {
			return mFiles.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView != null ? convertView : mInflater.inflate(R.layout.file_picker_list_item, parent, false);
			TextView name = (TextView) v.findViewById(R.id.nameTextView);

			File file = getItem(position);
			name.setText(file.getName());

			return v;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
		File file = (File) parent.getItemAtPosition(position);

		if (file.isDirectory()) {
			mPrevFolders.push(mCurrentFolder);
			loadFolder(file);
		} else {
			mDialog.dismiss();
			mActivity.uploadFile(file);
		}

	}

	@Override
	public void onClick(View arg0) {
		if (!mPrevFolders.isEmpty())
			loadFolder(mPrevFolders.pop());
	}
}
