package com.example.demodropbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

@SuppressLint("NewApi")
public class DropBoxActivity extends Activity implements OnClickListener, OnItemClickListener {

	// /////////////////////////////////////////////////////////////////////////
	// Your app-specific settings. //
	// /////////////////////////////////////////////////////////////////////////

	// Replace this with your app key and secret assigned by Dropbox.
	// Note that this is a really insecure way to do this, and you shouldn't
	// ship code which contains your key & secret in such an obvious way.
	// Obfuscation is good.
	final static private String		APP_KEY				= "nijajqwkpcg4lc9";
	final static private String		APP_SECRET			= "e7b7c8pthgvvaev";
	// /////////////////////////////////////////////////////////////////////////
	// End app-specific settings. //
	// /////////////////////////////////////////////////////////////////////////

	// You don't need to change these, leave them alone.
	final static private String		ACCOUNT_PREFS_NAME	= "prefs";
	final static private String		ACCESS_KEY_NAME		= "ACCESS_KEY";
	final static private String		ACCESS_SECRET_NAME	= "ACCESS_SECRET";

	private static final String		TAG					= "dan";
	private static final boolean	USE_OAUTH1			= false;

	DropboxAPI<AndroidAuthSession>	mDropboxApi;

	private boolean					mLoggedIn;

	private final String			DIR					= "/";
	private String					mFileName;

	private ArrayList<FileInfo>		mFiles;
	private ListView				mFileListView;

	// Controls

	private Button					mDownload;
	private Button					mUpload;
	private Builder					mBuilder;
	private AlertDialog				mDialog;
	private boolean					mIsDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// We create a new AuthSession so that we can use the Dropbox API.
		AndroidAuthSession session = buildSession();
		mDropboxApi = new DropboxAPI<AndroidAuthSession>(session);
		setContentView(R.layout.activity_drop_box);
		checkAppKeySetup();

		initControlValues();
		// Display the proper UI state if logged in or not
		setLoggedIn(mDropboxApi.getSession().isLinked());

	}

	private void initControlValues() {
		mDownload = (Button) findViewById(R.id.btn_downloads);
		mUpload = (Button) findViewById(R.id.btn_upload);

		mDownload.setOnClickListener(this);
		mUpload.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		AndroidAuthSession session = mDropboxApi.getSession();

		// The next part must be inserted in the onResume() method of the
		// activity from which session.startAuthentication() was called, so
		// that Dropbox authentication completes properly.
		if (session.authenticationSuccessful()) {
			try {
				// Mandatory call to complete the auth
				session.finishAuthentication();

				// Store it locally in our app for later use
				storeAuth(session);
				setLoggedIn(true);
			} catch (IllegalStateException e) {
				showToast("Couldn't authenticate with Dropbox:" + e.getLocalizedMessage());
				Log.i(TAG, "Error authenticating", e);
			}
		}

	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void storeAuth(AndroidAuthSession session) {
		// Store the OAuth 2 access token, if there is one.
		String oauth2AccessToken = session.getOAuth2AccessToken();
		if (oauth2AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, "oauth2:");
			edit.putString(ACCESS_SECRET_NAME, oauth2AccessToken);
			edit.commit();
			return;
		}
		// Store the OAuth 1 access token, if there is one. This is only
		// necessary if
		// you're still using OAuth 1.
		AccessTokenPair oauth1AccessToken = session.getAccessTokenPair();
		if (oauth1AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, oauth1AccessToken.key);
			edit.putString(ACCESS_SECRET_NAME, oauth1AccessToken.secret);
			edit.commit();
			return;
		}
	}

	private void checkAppKeySetup() {
		// Check to make sure that we have a valid app key
		if (APP_KEY.startsWith("CHANGE") || APP_SECRET.startsWith("CHANGE")) {
			showToast("You must apply for an app key and secret from developers.dropbox.com, and add them to the DBRoulette ap before trying it.");
			finish();
			return;
		}

		// Check if the app has set up its manifest properly.
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		String scheme = "db-" + APP_KEY;
		String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
		testIntent.setData(Uri.parse(uri));
		PackageManager pm = getPackageManager();
		if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
			showToast("URL scheme in your app's " + "manifest is not set up correctly. You should have a "
					+ "com.dropbox.client2.android.AuthActivity with the " + "scheme: " + scheme);
			finish();
		}
	}

	private void showToast(String msg) {
		Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		error.show();
	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0 || secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.drop_box, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.dropboxlink) {
			if (mLoggedIn) {
				logOut();
			} else {
				// Start the remote authentication
				if (USE_OAUTH1) {
					mDropboxApi.getSession().startAuthentication(DropBoxActivity.this);
				} else {
					mDropboxApi.getSession().startOAuth2Authentication(DropBoxActivity.this);
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item = menu.findItem(R.id.dropboxlink);
		if (mLoggedIn) {
			item.setTitle("Unlink from dropbox");

		} else
			item.setTitle("Link to dropbox");
		return super.onPrepareOptionsMenu(menu);
	}

	private void logOut() {
		// Remove credentials from the session
		mDropboxApi.getSession().unlink();

		// Clear our stored keys
		clearKeys();
		// Change UI state to display logged out version
		setLoggedIn(false);
	}

	private void clearKeys() {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		Editor edit = prefs.edit();
		edit.clear();
		edit.commit();
	}

	/**
	 * Convenience function to change UI state based on being logged in
	 */
	private void setLoggedIn(boolean loggedIn) {
		mLoggedIn = loggedIn;
		if (loggedIn) {
			// mSubmit.setText("Unlink from Dropbox");
			// mDisplay.setVisibility(View.VISIBLE);
			invalidateOptionsMenu();
		} else {
			// mSubmit.setText("Link with Dropbox");
			// mDisplay.setVisibility(View.GONE);
			// mImage.setImageDrawable(null);
			invalidateOptionsMenu();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		// Download image from dropbox open dropbox files
		case R.id.btn_downloads:
			if (mLoggedIn)
				showFilesFromDropBox(DIR);
			else
				showToast("You have to log in for access");
			break;
		// Upload from sdcard to dropbox open sdcard
		case R.id.btn_upload:
			// Open file picker
			new FilePicker(this);
			break;
		}

	}

	public void setDialogForFileList() {
		mBuilder = new Builder(this);
		mBuilder.setTitle("Choose file to download");
		View view = getLayoutInflater().inflate(R.layout.listview, null);
		mBuilder.setView(view);
		mFileListView = (ListView) view.findViewById(R.id.listView1);
		mFileListView.setOnItemClickListener(this);
		mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub

			}
		});
	}

	public ListView getListView() {
		return mFileListView;
	}

	private void showFilesFromDropBox(String filepath) {
		mFiles = new ArrayList<FileInfo>();
		ShowDropBoxFiles showFiles = new ShowDropBoxFiles(mDropboxApi, filepath, this, mFiles);
		showFiles.execute();

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		String selectedFilePath = mFileListView.getItemAtPosition(position).toString();
		String filepath = null;
		for (FileInfo fi : mFiles) {
			if (fi.getFileName().equals(selectedFilePath)) {
				filepath = fi.getFilePath();
				mFileName = fi.getFileName();
			}
		}
		showFilesFromDropBox(filepath);

	}

	public void downLoadFile(String path) {
		dismissDialog();
		File directory = new File(Environment.getExternalStorageDirectory() + "/");
		if (!directory.exists()) {
			directory.mkdir();
		}
		String targetpath = Environment.getExternalStorageDirectory() + "/" + mFileName;
		final File importedFile = new File(targetpath);
		if (!importedFile.exists())
			importedFile.mkdir();
		OutputStream out;
		try {
			out = new FileOutputStream(importedFile);
			if (out != null) {
				DownloadFile download = new DownloadFile(this, mDropboxApi, path, out);
				download.execute();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public void uploadFile(File file) {
		UploadFile upload = new UploadFile(this, mDropboxApi, file, DIR);
		upload.execute();
	}

	public boolean getDialogStatus() {
		return mIsDialog;
	}

	public void dismissDialog() {
		mIsDialog = false;
		mDialog.dismiss();

	}

	public void showDialog() {
		mIsDialog = true;
		mDialog = mBuilder.create();
		mDialog.show();
	}

}
